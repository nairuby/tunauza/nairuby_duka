var messageDelay = 2000;  // How long to display status messages (in milliseconds)

// Init the form once the document is ready
$( init );


// Initialize the form

function init() {
  $("#contactForm").submit(function(e){
    e.preventDefault();
  });

  $('#send').click(function() {
    submitForm();
  });
}

// Submit the form via Ajax

function submitForm() {
  var contactForm = $('#contactForm');

  // Are all the fields filled in?
  cname = $('#name');
  pnumber = $('#phone_number');
  email = $('#email');
  message = $('#message');
  subject = $('#subject');

  if ( !cname.val() || !email.val() || !message.val() || !subject.val() ) {

    // No; display a warning message and return to the form
    $('#contact-errormessage').fadeIn().delay(messageDelay).fadeOut();
    contactForm.fadeOut().delay(messageDelay).fadeIn();

  } else {

    // Yes; submit the form to the PHP script via Ajax

    $('#sendingMessage').fadeIn();
    contactForm.fadeOut();

    var data = 'name=' + cname.val() + '&subject=[Simba Machinery WebContact]: ' + subject.val() +'&email=' + email.val() +
        '&message='  + encodeURIComponent(message.val());

    $.ajax( {
      url: "http://send-email.bantatechnologies.com/simba_send_email",
      type: "POST",
      data: data,
      success: submitFinished
    } );
  }

  // Prevent the default form submission occurring
  return false;
}


// Handle the Ajax response

function submitFinished( response ) {
  response = response['message']
  $('#sendingMessage').fadeOut();

  if ( response == "success" ) {

    // Form submitted successfully:
    // 1. Display the success message
    // 2. Clear the form fields
    // 3. Fade the content back in

    $('#contact-successmessage').fadeIn().delay(messageDelay).fadeOut();
    $('#name').val( "" );
    $('#phonenumber').val( "" );
    $('#email').val( "" );
    $('#message').val( "" );
    $('#subject').val( "" );

    $('#contactForm').delay(messageDelay+500).fadeIn();

  } else {

    // Form submission failed: Display the failure message,
    // then redisplay the form
    $('#contact-errormessage').fadeIn().delay(messageDelay).fadeOut();
    $('#contactForm').delay(messageDelay+500).fadeIn();
  }
}
