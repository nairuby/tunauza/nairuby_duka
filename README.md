# Nairuby Online Shop

Welcome to Nairuby Online Shop! Nairuby Online Shop is a project developed by Nairuby to provide a platform for selling Nairuby community merchandise. This readme file provides essential information about the project and how to set it up locally.
## Table of Contents

- [Nairuby Online Shop](#nairuby-online-shop)
  - [Table of Contents](#table-of-contents)
  - [Local Setup Guide](#local-setup-guide)
    - [Prerequisites](#prerequisites)
    - [Installation](#installation)
  - [Chat on Gitter](#chat-on-gitter)

## Local Setup Guide

To run Nairuby Online Shop on your local machine, follow these steps:

### Prerequisites

Before you begin, make sure you have the following installed on your system:

- Ruby (version 2.7 or higher)
- Bundler gem

### Installation

```bash
git clone https://gitlab.com/nairuby/tunauza/nairuby_duka
cd nairuby_duka
bundle config set --local path 'vendor/bundle'
bundle install
bundle update
bundle exec ruby _build/preprocess_assets.rb
bundle exec jekyll build -d public
```


## Chat on Gitter

Join the community on Gitter to discuss Nairuby Online Shop, ask questions, seek help, share ideas, and collaborate on the development of the project.

[![Gitter](https://badges.gitter.im/tunauza/community.svg)](https://gitter.im/tunauza/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

---


Thank you for your interest in Nairuby Online Shop! If you encounter any issues or have any questions, feel free to [create an issue](https://gitlab.com/nairuby/tunauza/nairuby_duka/issues) on our GitLab repository. Happy coding!
